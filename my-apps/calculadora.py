import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class JanelaComGrade(Gtk.Window):
    def __init__(self):
        
        super().__init__(title = "Calculadora")
        
        self.texto = Gtk.Entry()
        self.texto.set_text("787923")
        self.texto.set_alignment(1)
        
        # Números
        button0 = Gtk.Button(label = "0")
        button1 = Gtk.Button(label = "1")
        button2 = Gtk.Button(label = "2")
        button3 = Gtk.Button(label = "3")
        button4 = Gtk.Button(label = "4")
        button5 = Gtk.Button(label = "5")
        button6 = Gtk.Button(label = "6")
        button7 = Gtk.Button(label = "7")
        button8 = Gtk.Button(label = "8")
        button9 = Gtk.Button(label = "9")
        
        # Operações e vírgula
        buttonComma           = Gtk.Button(label = ",")
        buttonPercentage      = Gtk.Button(label = "%")
        buttonPlus            = Gtk.Button(label = "+")
        buttonMinus           = Gtk.Button(label = "-")
        buttonMultiply        = Gtk.Button(label = "x")
        buttonDivide          = Gtk.Button(label = "÷")
        buttonEqual           = Gtk.Button(label = "=")
        buttonExp2            = Gtk.Button(label = "x²")
        buttonSquareRoot      = Gtk.Button(label = "√")
        buttonOpenParentesis  = Gtk.Button(label = "(")
        buttonCloseParentesis = Gtk.Button(label = ")")
        buttonUndo            = Gtk.Button(label = "Desfaz")
        buttonClear           = Gtk.Button(label = "Limpa")
        
        grid = Gtk.Grid()
        grid.attach(self.texto, 1, 0, 6, 1)
        grid.attach_next_to(button7, self.texto, Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(button8, button7, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(button9, button8, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonDivide, button9, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonUndo, buttonDivide, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonClear, buttonUndo, Gtk.PositionType.RIGHT, 1, 1)

        grid.attach_next_to(button4, button7, Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(button5, button4, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(button6, button5, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonMultiply, button6, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonOpenParentesis, buttonMultiply, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonCloseParentesis, buttonOpenParentesis, Gtk.PositionType.RIGHT, 1, 1)

        grid.attach_next_to(button1, button4, Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(button2, button1, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(button3, button2, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonMinus, button3, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonExp2, buttonMinus, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonSquareRoot, buttonExp2, Gtk.PositionType.RIGHT, 1, 1)

        grid.attach_next_to(button0, button1, Gtk.PositionType.BOTTOM, 1, 1)
        grid.attach_next_to(buttonComma, button0, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonPercentage, buttonComma, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonPlus, buttonPercentage, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach_next_to(buttonEqual, buttonPlus, Gtk.PositionType.RIGHT, 2, 1)
        
        self.add(grid)

janela = JanelaComGrade()
janela.connect("destroy", Gtk.main_quit)
janela.show_all()
Gtk.main()
