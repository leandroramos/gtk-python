import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class MinhaJanela(Gtk.Window):
    def __init__(self):
        super().__init__(title="Olá, Mundo!")
        
        self.vbox = Gtk.VBox()
        
        self.label = Gtk.Label("Clique no botão abaixo...")
        
        self.button = Gtk.Button(label="Clique aqui")
        self.button.connect("clicked", self.on_button_clicked)
        
        self.vbox.pack_start(self.label, False, False, 8)
        self.vbox.pack_start(self.button, False, False, 8)
        
        self.add(self.vbox)
        
    def on_button_clicked(self, widget):
        print("Olá, Mundo!")
        self.label.set_text("Você clicou no botão!!!!")

janela = MinhaJanela()
janela.connect("destroy", Gtk.main_quit)
janela.show_all()
Gtk.main()
