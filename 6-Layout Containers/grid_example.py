import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class JanelaComGrade(Gtk.Window):
    def __init__(self):
        
        super().__init__(title = "Grid Example")
        
        button1 = Gtk.Button(label = "Botão 1")
        button2 = Gtk.Button(label = "Botão 2")
        button3 = Gtk.Button(label = "Botão 3")
        button4 = Gtk.Button(label = "Botão 4")
        button5 = Gtk.Button(label = "Botão 5")
        button6 = Gtk.Button(label = "Botão 6")
        
        grid = Gtk.Grid()
        grid.add(button1)
        grid.attach(button2, 1, 0, 2, 1)
        grid.attach_next_to(button3, button1, Gtk.PositionType.BOTTOM, 1, 2)
        grid.attach_next_to(button4, button3, Gtk.PositionType.RIGHT, 2, 1)
        grid.attach(button5, 1, 2, 1, 1)
        grid.attach_next_to(button6, button5, Gtk.PositionType.RIGHT, 1, 1)
        
        self.add(grid)

janela = JanelaComGrade()
janela.connect("destroy", Gtk.main_quit)
janela.show_all()
Gtk.main()
