import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class MinhaJanela(Gtk.Window):
    def __init__(self):
        super().__init__(title = "Exemplo de Notebook")
        self.set_border_width(3)

        self.notebook = Gtk.Notebook()
        self.add(self.notebook)

        self.page1 = Gtk.Box()
        self.page1.set_border_width(10)
        self.page1.add(Gtk.Label(label = "Página Padrão!"))
        self.notebook.append_page(
            self.page1,
            Gtk.Label(label = "Título de texto")
        )

        self.page2 = Gtk.Box()
        self.page2.set_border_width(10)
        self.page2.add(Gtk.Label(label = "Uma página com uma imagem no título."))
        self.notebook.append_page(
            self.page2,
            Gtk.Image.new_from_icon_name("help-about", Gtk.IconSize.MENU)
        )

janela = MinhaJanela()
janela.connect("destroy", Gtk.main_quit)
janela.show_all()
Gtk.main()
